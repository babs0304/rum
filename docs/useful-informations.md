## Symfony utils

- Create database: `symfony make:docker:database`. If errro on Dumper, change number of space in docker-compose.yml file.
- Creation des entity: `symfony console make:entity`
- Creation des migration: `symfony console make:migration`
- Appliquer la migration: `symfony console d:m:m`

## GITLab API

- curl "https://gitlab.com/api/v4/projects"
https://gitlab.com/babs0304/rum/-/issues/9


PMO-API Read Only token: glpat-wDsnM6ybUqgLP8qpFydA

- RUM Project id : 56865325

curl --header "PRIVATE-TOKEN: glpat-wDsnM6ybUqgLP8qpFydA" --url "https://gitlab.com/api/v4/projects/56865325/issues"
curl --header "PRIVATE-TOKEN: <your_access_token>" \  --url "https://gitlab.example.com/api/v4/issues"

curl "https://gitlab.com/api/v4/projects?access_token=wDsnM6ybUqgLP8qpFydA"

projects/56865325/issues