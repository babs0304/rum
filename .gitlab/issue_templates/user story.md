# Description

**As a** <who ?>

**I want to** <what ?>

**in order to** <why ?>

## Additional information
 (e.g. related page, screen, url, and so on..)

## Acceptance Criteria

### Scenario 1 (e.g. Successful User Registration)

- **Given** <situation> (e.g. the user is on the registration page)
- **When** <criteria/obective> (e.g. the user enters valid name, email, and password)
- **And** <other criteria/objective> (e.g. clicks on the "Register" button)
- **Then** <expected result> (e.g the user should be redirected to the login page)

/label ~"User Story" ~Feature