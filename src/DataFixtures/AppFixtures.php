<?php

namespace App\DataFixtures;

use App\Entity\Candidate;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $candidate = new Candidate();
        $candidate->setFirstName('Michael');
        $candidate->setLastName('Staudenmann');

        $date = DateTimeImmutable::createFromFormat("Y-m-d", "1981-08-31");
        $candidate->setBirthDate($date);

        $candidate->setNationality("Suisse");
        $manager->persist($candidate);
        $manager->flush();
    }
}
