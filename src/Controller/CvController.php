<?php

namespace App\Controller;

use App\Entity\Candidate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints\Length;

class CvController extends AbstractController
{
    #[Route('/cv', name: 'app_cv')]
    public function index(EntityManagerInterface $manager): Response
    {
        $candidates = $manager->getRepository(Candidate::class)->findAll();
        if (count($candidates) > 0) {
            return $this->render('cv/index.html.twig', [
                'controller_name' => 'CvController',
                'name' => $candidates[0]->getFirstName(),
                'dob' => $candidates[0]->getBirthDate()
            ]);
        }
    }
}
